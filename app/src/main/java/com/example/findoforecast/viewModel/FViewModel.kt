package com.example.findoforecast.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.findoforecast.helper.FHelper
import com.example.findoforecast.model.FForecast
import com.example.findoforecast.network.forecast.ForecastAPI
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class FViewModel : ViewModel() {

    val getForecast = ForecastAPI()
    private var listForecast: MutableList<FForecast> = mutableListOf()
    private val listForecastData = MutableLiveData<MutableList<FForecast>>()

    fun setListForecastData(sectionList: MutableList<FForecast>) {
        listForecastData.value = sectionList
    }

    fun getForecastList(citySelected: String) {
        doAsync {
            getForecast.getForecast(citySelected).subscribe({ forecastCity ->
                uiThread {
                    FHelper().setLimitList(listForecast, forecastCity)
                    setListForecastData(listForecast.asReversed())
                }
            }) { Throwable ->
                print(Throwable.toString())
            }
        }
    }

    fun getForecastListLiveData(citySelected: String): LiveData<MutableList<FForecast>> {
        getForecastList(citySelected)
        return listForecastData
    }
}