package com.example.findoforecast.model

import com.google.gson.annotations.SerializedName

data class FForecast (

	@SerializedName("coord") val coord : Coord,
	@SerializedName("weather") val weather : List<Weather>,
	@SerializedName("main") val main : Main,
	@SerializedName("id") val id : Int,
	@SerializedName("name") val name : String,
	@SerializedName("cod") val cod : Int
)