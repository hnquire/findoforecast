package com.example.findoforecast.network.forecast

import com.example.findoforecast.network.RetrofitClient
import com.example.findoforecast.model.FForecast
import com.example.findoforecast.utils.Constant
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ForecastAPI {

    private var myCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getForecast(citySelected: String): Single<FForecast> {
        val retrofit = RetrofitClient.instance
        val requestInterface = retrofit.create(GetForecastData::class.java)
        val sectionsRequest = requestInterface.getForecastData("weather?q=$citySelected&APPID=${Constant().APPKEY}")
        myCompositeDisposable.add(
            sectionsRequest
                .retry(2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ sectionsData ->
                    sectionsRequest.doOnSuccess {  }
                }){ Throwable ->
                    print(Throwable.toString())
                }
        )
        return sectionsRequest
    }
}