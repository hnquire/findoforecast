package com.example.findoforecast.network.forecast

import com.example.findoforecast.model.FForecast
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface GetForecastData {

    @GET
    fun getForecastData(@Url url: String): Single<FForecast>
}