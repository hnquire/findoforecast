package com.example.findoforecast.utils

import android.content.Context
import androidx.room.Room
import com.example.findoforecast.db.ForecastDb
import com.example.findoforecast.model.FForecast
import com.example.findoforecast.db.FForecastEntity

class DbHelper(context: Context) {
    val db = Room.databaseBuilder(
        context,
        ForecastDb::class.java,
        "ForecastDB"
    ).build()

    fun setListToDb(forecastList: MutableList<FForecast>){
        for (workout in forecastList) {
            db.forecastDao().saveNewForecast(workout as FForecastEntity)
        }
    }
}