package com.example.findoforecast.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.findoforecast.R
import com.example.findoforecast.model.FForecast
import com.example.findoforecast.utils.Constant
import com.example.findoforecast.utils.Helpers
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.forecast_card.view.*

class ForecastAdapter(val forecasts: MutableList<FForecast>) : RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {

    private var positionObservable = PublishSubject.create<Int>()
    var forecastObserver: Observable<Int> = positionObservable

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        return ForecastViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.forecast_card, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return forecasts.size
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        val forecast = forecasts[position]
        // reformat Kelvin to Celsius
        val temperatureCelsius = Helpers(forecast.main.temp).toCelsius()
        val maxTempCesius = Helpers(forecast.main.temp_max).toCelsius()
        val minTempCesius = Helpers(forecast.main.temp_min).toCelsius()

        holder.forecast_view.tv_weather.text = forecast.weather[0].main
        holder.forecast_view.tv_weather_desc.text = forecast.weather[0].description
        holder.forecast_view.tv_city.text = forecast.name

        holder.forecast_view.tv_temperature.text = temperatureCelsius.toInt().toString() + "°C"
        holder.forecast_view.tv_pressure.text = forecast.main.pressure.toString() + "hpa"
        holder.forecast_view.tv_max_temperature.text = maxTempCesius.toInt().toString() + "°C"
        holder.forecast_view.tv_min_temperature.text = minTempCesius.toInt().toString() + "°C"
        holder.forecast_view.tv_humidity.text = forecast.main.humidity.toString() + "%"

        if (forecast.weather[0].icon != null) {
            holder.forecast_view.iv_weather_icon.visibility = (View.VISIBLE)

            val imageUrl = Constant().baseIconURL + forecast.weather[0].icon + "@2x.png"
            Picasso.get().load(imageUrl).placeholder(R.drawable.weather_icon).error(R.drawable.weather_icon)
                .into(holder.forecast_view.iv_weather_icon)
        } else {
            holder.forecast_view.iv_weather_icon.visibility = (View.GONE)
        }

        holder.forecast_view.setOnClickListener {
            positionObservable.onNext(position)
        }
    }

    class ForecastViewHolder(val forecast_view: View) : RecyclerView.ViewHolder(forecast_view)

    fun removeItem(viewHolder: RecyclerView.ViewHolder){
        forecasts.removeAt(viewHolder.adapterPosition)
        notifyItemRemoved(viewHolder.adapterPosition)
    }
}