package com.example.findoforecast.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database (entities = [(FForecastEntity::class)], version = 1)
abstract class ForecastDb : RoomDatabase(){
    abstract fun forecastDao(): FForecastDAO
}