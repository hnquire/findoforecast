package com.example.findoforecast.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface FForecastDAO {

    @Insert
    fun saveNewForecast(forecast: FForecastEntity)

    @Query("select * from fforecastentity")
    fun forecast(): List<FForecastEntity>


}