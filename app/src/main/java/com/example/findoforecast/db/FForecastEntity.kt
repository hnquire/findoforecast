package com.example.findoforecast.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class FForecastEntity {

    @PrimaryKey
    var id: Int? = null

    @ColumnInfo(name = "ffore_coord")
    var coord: String? = null

    @ColumnInfo(name = "ffore_weather")
    var weather: String? = null

    @ColumnInfo(name = "ffore_main")
    var main: String? = null

    @ColumnInfo(name = "ffore_nameForecast")
    var nameForecast: String? = null

    @ColumnInfo(name = "ffore_cod")
    var cod: Int? = null
}