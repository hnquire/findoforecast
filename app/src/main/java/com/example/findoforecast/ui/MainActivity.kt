package com.example.findoforecast.ui

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.findoforecast.R
import com.example.findoforecast.adapter.ForecastAdapter
import com.example.findoforecast.db.FForecastEntity
import com.example.findoforecast.db.ForecastDb
import com.example.findoforecast.model.FForecast
import com.example.findoforecast.utils.Constant
import com.example.findoforecast.viewModel.FViewModel
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mForecastList: MutableList<FForecast> = mutableListOf()
    private lateinit var viewModel: FViewModel
    private lateinit var map: GoogleMap

    private val forecastAdapter = ForecastAdapter(mForecastList)

    var placeFields = Arrays.asList(
        Place.Field.NAME
    )

    lateinit var placesClient: PlacesClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map_forecast) as SupportMapFragment
        mapFragment.getMapAsync(this)

        viewModel = ViewModelProviders.of(this).get(FViewModel::class.java)

        //db creation incomplete
//        Thread{
//            val db = Room.databaseBuilder(
//                applicationContext,
//                ForecastDb::class.java,
//                "ForecastDB"
//            ).build()
//
//            val forecastdb = FForecastEntity()
//            for (forecast in mForecastList){
//
//                // Add Converters
//                forecastdb.nameForecast = forecast.name
//                forecastdb.cod = forecast.cod
//                forecastdb.coord = forecast.coord.toString()
//                forecastdb.id = forecast.id
//                forecastdb.main = forecast.main.toString()
//                forecastdb.weather = forecast.weather.toString()
//
//                db.forecastDao().saveNewForecast(FForecastEntity())
//            }
//
//            //Show db and Parse MutableList
//            db.forecastDao().forecast().forEach{
//                // Add parce from Db To MutableList
//            }
//        }

        //init Place API in Fragment
        initPlaces()
        setupPlacesAutocomplete()

        // Set remove items by Swipe
        val itemTouchHelperCallback =
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int) {
                    ForecastAdapter(mForecastList).removeItem(viewHolder)
                    setAdapterForecastData()
                }
            }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(rv_forecast)
    }

    private fun setupPlacesAutocomplete() {
        val autocompleteFragment = supportFragmentManager
            .findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(placeFields)
        autocompleteFragment.setTypeFilter(TypeFilter.CITIES)

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                //City Selected and API call
                getForecastList(p0.name!!)
            }

            override fun onError(p0: Status) {
                Toast.makeText(this@MainActivity, "" + p0.statusMessage, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun initPlaces() {
        Places.initialize(this, Constant().apiPlacesKey)
        placesClient = Places.createClient(this)
    }

    fun getForecastList(citySelected: String) {

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Cargando...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val sectionsObserver = Observer<MutableList<FForecast>> {
            mForecastList = it

            //Insert list to db

            setAdapterForecastData()
            progressDialog.dismiss()
        }

        viewModel.getForecastListLiveData(citySelected).observe(this, sectionsObserver)
    }

    private fun setAdapterForecastData() {
        rv_forecast.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_forecast.setHasFixedSize(true)
        rv_forecast.adapter = ForecastAdapter(mForecastList)

        // Fix Helper
//            val callback: ItemTouchHelper.Callback = ForecastItemTouchHelper(ForecastAdapter(mForecastList))
//            val itemTouchHelper = ItemTouchHelper(callback)
//            forecastAdapter.setTouchHelper(itemTouchHelper)
//            itemTouchHelper.attachToRecyclerView(rv_forecast)
//            print(mForecastList)


        // set last item in map
        if (mForecastList.size != 0) {
            val selectedItemFromList = mForecastList.asReversed()[(mForecastList.size - 1)]
            val coordinate = LatLng((selectedItemFromList.coord.lat), (selectedItemFromList.coord.lon))
            setUpMap(coordinate, selectedItemFromList.name)
        }

        forecastAdapter.forecastObserver.subscribe { position ->
            var itemSelected = mForecastList[position]
            val coordinate = LatLng((itemSelected.coord.lat), (itemSelected.coord.lon))
            setUpMap(coordinate, itemSelected.name)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
    }

    private fun setUpMap(coordinate: LatLng, cityName: String) {
        map.clear()
        map.addMarker(MarkerOptions().position(coordinate).title("Marker in $cityName"))
        map.moveCamera(CameraUpdateFactory.newLatLng(coordinate))
    }
}